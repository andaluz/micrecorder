//
//  AudioRecorder.m
//  MicRecorder
//
//  Created by Nordin on 13/04/2018.
//  Copyright © 2018 Digivox. All rights reserved.
//
//  As you can see, I renamed AudioRecorder.m to AudioRecorder.mm,
//  So it becomes a kind of Objective-C++ file, this is my first time too.
//  I hope it at least compiles.


#import "AudioRecorder.h"
#import <AudioToolbox/AudioToolbox.h>


// source: https://developer.apple.com/library/content/documentation/MusicAudio/Conceptual/AudioQueueProgrammingGuide/AQRecord/RecordingAudio.html

// **** How it works ****
// First you have to set up some stuff, like how many buffers to use,
// what samplerate, codec, size of buffers and so on...
// Than you need a callback when everytime a buffer is filled, the audio engine
// will call your callback to deliver the recorded data.
// After you get the recorded data, you process it and than put it
// back in the Audio Queue, which is called 'Enqueue Buffer'.
// For that you call the method AudioQueueEnqueueBuffer(...);


// Since we use Mic input, we'll use only 2 buffers,
// 1 for sending to an output and concurrently 1 for reading new sample data
static const int kNumberBuffers = 2;

// This state structure is appearantly needed acoording to Apple's documentations
struct AQRecorderState {
    AudioStreamBasicDescription  mDataFormat;                   // info about what audio format (pcm, ulaw, alaw..)
    AudioQueueRef                mQueue;                        // The queue that is provided by our app (list of buffers)
    AudioQueueBufferRef          mBuffers[kNumberBuffers];      // pointers to buffers (in our case just 2)
    AudioFileID                  mAudioFile;                    // For our purpose not needed.
    UInt32                       bufferByteSize;                // In our case, buffer must contain 20 ms of sample data (8000/20=400)
    SInt64                       mCurrentPacket;                // Packet index in the current buffer, next buffer has index = index+1
    bool                         mIsRunning;                    // Indicates whether Audio Queue is running or not
};

// Callback that will be called by AudioQueue
static void HandleInputBuffer (
                               void                                 *aqData,        // Contains info described above
                               AudioQueueRef                        inAQ,           // The AudioQueue from where this callback is called
                               AudioQueueBufferRef                  inBuffer,       // Buffer that is filled with sampled audio (i.e. from the Mic)
                               const AudioTimeStamp                 *inStartTime,   // Timestamp of first sample (I guess per buffer)
                               UInt32                               inNumPackets,   // Number of packed descriptions, 0 is CBR (for simplicity)
                               const AudioStreamPacketDescription   *inPacketDesc   // For some compressed audio format description (I guess like mp3)
                               ) {
    
    
    AQRecorderState *pAqData = (AQRecorderState *) aqData;       // 1
    
    if (inNumPackets == 0 &&                                     // 2 in our situation it should only be 1
        pAqData->mDataFormat.mBytesPerPacket != 0)
        inNumPackets =
        inBuffer->mAudioDataByteSize / pAqData->mDataFormat.mBytesPerPacket;
                                   
    // To read audio data, use: inBuffer->mAudioData

    pAqData->mCurrentPacket += inNumPackets;                     // 4

    if (pAqData->mIsRunning == 0)                                // 5
        return;
    
    AudioQueueEnqueueBuffer (                                    // 6
                             pAqData->mQueue,
                             inBuffer,
                             0,
                             NULL
                             );
}

// In this method you actually configure the Buffer size.
// Trial&Error is the only way to figure out the right configuration.
// The reason, I don't have a f$%#&@ clue what PacketSize actually is and CBR/VBR!
void DeriveBufferSize (
                       AudioQueueRef                audioQueue,                  // 1
                       AudioStreamBasicDescription  &ASBDescription,             // 2
                       Float64                      seconds,                     // 3
                       UInt32                       *outBufferSize               // 4
                      ) {
    static const int maxBufferSize = 160;                       // For a recording with 8000 samples/sec --> 160 samples/20ms
    
    int maxPacketSize = ASBDescription.mBytesPerPacket;             // 6
    /*
    if (maxPacketSize == 0) {                                       // 7
        UInt32 maxVBRPacketSize = sizeof(maxPacketSize);
        AudioQueueGetProperty (
                               audioQueue,
                               kAudioQueueProperty_MaximumOutputPacketSize,
                               // in Mac OS X v10.5, instead use
                               //   kAudioConverterPropertyMaximumOutputPacketSize
                               &maxPacketSize,
                               &maxVBRPacketSize
                               );
    }
    */
    Float64 numBytesForTime =
    ASBDescription.mSampleRate * maxPacketSize * seconds;           // 8
    *outBufferSize =
    UInt32 (numBytesForTime < maxBufferSize ?
            numBytesForTime : maxBufferSize);                       // 9
}

@implementation AudioRecorder


- (void) startAudioRecording {
    // First, let's setup how we want to record audio
    AQRecorderState aqData;                                         // 1
    
    aqData.mDataFormat.mFormatID         = kAudioFormatALaw;        // 2
    aqData.mDataFormat.mSampleRate       = 8000.0;                  // 3
    aqData.mDataFormat.mChannelsPerFrame = 1;                       // 4, we have only mono
    aqData.mDataFormat.mBitsPerChannel   = 8;                       // 5, no stereo stuff
    aqData.mDataFormat.mBytesPerPacket   = 1;                       // 6
    aqData.mDataFormat.mBytesPerFrame    = 1;
    aqData.mDataFormat.mChannelsPerFrame * sizeof (SInt8);
    aqData.mDataFormat.mFramesPerPacket  = 1;                       // 7
    
    //AudioFileTypeID fileType             = kAudioFileAIFFType;    // 8
    //aqData.mDataFormat.mFormatFlags =                             // 9
    //kLinearPCMFormatFlagIsBigEndian
    //| kLinearPCMFormatFlagIsSignedInteger
    //| kLinearPCMFormatFlagIsPacked;
    
    // Next, tell AudioQueue to do his job
    AudioQueueNewInput (                              // 1
                        &aqData.mDataFormat,                          // 2
                        HandleInputBuffer,                            // 3
                        &aqData,                                      // 4
                        NULL,                                         // 5, runs in ann Audio thread
                        kCFRunLoopCommonModes,                        // 6, the thread to use for the callback
                        0,                                            // 7, reserved and must be zero!
                        &aqData.mQueue                                // 8, a queue to be allocated
                        );
}

@end
