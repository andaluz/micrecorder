//
//  MicRecorderVC.m
//  MicRecorder
//
//  Created by Nordin on 13/04/2018.
//  Copyright © 2018 AppPlanet. All rights reserved.
//

#import "MicRecorderVC.h"
#import <AVFoundation/AVFoundation.h>

@interface MicRecorderVC ()

@end

@implementation MicRecorderVC

#pragma mark @synthesize
@synthesize player;
@synthesize recorder;
@synthesize playbackWasInterrupted;

#pragma mark Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init stuff...
    [self doSomeInitialization];
}

- (void) doSomeInitialization {
    // Allocate our singleton instance for the recorder & player object
    recorder = new AQRecorder();
    player = new AQPlayer();
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackQueueStopped:) name:@"playbackQueueStopped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackQueueResumed:) name:@"playbackQueueResumed" object:nil];
}

#pragma mark IBAction handlers
- (IBAction)didPressRecord:(UIButton *)sender {
    NSLog(@"MicRecorderVC - didPressRecord");
    
    if (recorder->IsRunning()) {// If we are currently recording, stop and save the file.
        [self stopRecord];
    }
    else {// If we're not recording, start.
        // an intance of AVAudioSession class
        AVAudioSession *session = [AVAudioSession sharedInstance];
        // we do not want to allow recording if input is not available
        [session requestRecordPermission:^(BOOL granted) {
            if (granted) {
                // microphone has permision to be used in this app
                NSError *error;
                // setting the category will also request access from the user
                [session setCategory:AVAudioSessionCategoryRecord error:&error];
                if (error)
                    NSLog(@"Couldn't set record audio category. Error=%ld", (long)error);
                
                [session setActive:YES error:&error];
                if (error)
                    NSLog(@"AVAudioSession setActive:YES failed in recording. Erorr=%ld", (long)error);
                
            }else {
                // Handle failure
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Microphone"
                                                                    message:@"Its access is denied"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                });
            }
        }];
        
        if (session.inputAvailable) {
            _btnPlay.enabled = NO;
            
            // Set the button's state to "stop"
            _btnRecord.titleLabel.text = @"Stop";
            
            // Start the recorder
            recorder->StartRecord(CFSTR("recordedFile.caf"));
            
            [self setFileDescriptionForFormat:recorder->DataFormat() withName:@"Recorded File"];
        }
    }
}

- (IBAction)didPressStop:(UIButton *)sender {
    NSLog(@"MicRecorderVC - didPressStop");
    
    [self stopRecord];
}

- (IBAction)didPressPlay:(UIButton *)sender {
    NSLog(@"MicRecorderVC - didPressPlay");
    
    NSError *error;// = [[NSError alloc] init];
    
    // setting playback category for listen to audio track without mixing audio from other apps
    @try {
        // an intance of AVAudioSession class
        AVAudioSession *session = [AVAudioSession sharedInstance];
        
        // it changes category for listen to track
        if(![session setCategory:AVAudioSessionCategoryPlayback error:&error]) {
            NSLog(@"Couldn't set playback audio category: %@", error.localizedDescription);
        }
        
        // the session must be active
        if(![session setActive:YES error:&error]) {
            NSLog(@"Couldn't set audio session active in playing: %@", error.localizedDescription);
        }
        
    } @catch (NSException *e) {
        NSLog(@"didPressPlay: %@", e.description);
        return;
    }
    
    if (!error) {
        // play
        if (player->IsRunning())
        {
            if (playbackWasPaused) {
                OSStatus result = player->StartQueue(true);
                playbackWasPaused = NO;
                if (result == noErr)
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"playbackQueueResumed" object:self];
            }
            else
                [self stopPlayQueue];
        }
        else
        {
            OSStatus result = player->StartQueue(false);
            if (result == noErr)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"playbackQueueResumed" object:self];
        }
    }
}

#pragma mark AudioSession listeners
void interruptionListener(void *    inClientData,
                          UInt32    inInterruptionState)
{
    MicRecorderVC *THIS = (__bridge MicRecorderVC*)inClientData;
    if (inInterruptionState == kAudioSessionBeginInterruption)
    {
        if (THIS->recorder->IsRunning()) {
            [THIS stopRecord];
        }
        else if (THIS->player->IsRunning()) {
            //the queue will stop itself on an interruption, we just need to update the UI
            [[NSNotificationCenter defaultCenter] postNotificationName:@"playbackQueueStopped" object:THIS];
            THIS->playbackWasInterrupted = YES;
        }
    }
    else if ((inInterruptionState == kAudioSessionEndInterruption) && THIS->playbackWasInterrupted)
    {
        // we were playing back when we were interrupted, so reset and resume now
        THIS->player->StartQueue(true);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playbackQueueResumed" object:THIS];
        THIS->playbackWasInterrupted = NO;
    }
}

void propListener(void *                  inClientData,
                  AudioSessionPropertyID    inID,
                  UInt32                  inDataSize,
                  const void *            inData)
{
    MicRecorderVC *THIS = (__bridge MicRecorderVC*)inClientData;
    if (inID == kAudioSessionProperty_AudioRouteChange)
    {
        CFDictionaryRef routeDictionary = (CFDictionaryRef)inData;
        //CFShow(routeDictionary);
        CFNumberRef reason = (CFNumberRef)CFDictionaryGetValue(routeDictionary, CFSTR(kAudioSession_AudioRouteChangeKey_Reason));
        SInt32 reasonVal;
        CFNumberGetValue(reason, kCFNumberSInt32Type, &reasonVal);
        if (reasonVal != kAudioSessionRouteChangeReason_CategoryChange)
        {
            if (reasonVal == kAudioSessionRouteChangeReason_OldDeviceUnavailable)
            {
                if (THIS->player->IsRunning()) {
                    [THIS pausePlayQueue];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"playbackQueueStopped" object:THIS];
                }
            }
            
            // stop the queue if we had a non-policy route change
            if (THIS->recorder->IsRunning()) {
                [THIS stopRecord];
            }
        }
    }
    else if (inID == kAudioSessionProperty_AudioInputAvailable)
    {
        if (inDataSize == sizeof(UInt32)) {
            UInt32 isAvailable = *(UInt32*)inData;
            // disable recording if input is not available
            THIS->_btnPlay.enabled = (isAvailable > 0) ? YES : NO;
        }
    }
}

#pragma mark other methods
- (void) stopRecord {
    recorder->StopRecord();
    
    // dispose the previous playback queue
    player->DisposeQueue(true);
    
    // now create a new queue for the recorded file
    recordFilePath = (__bridge CFStringRef)[NSTemporaryDirectory() stringByAppendingPathComponent: @"recordedFile.caf"];
    player->CreateQueueForFile(recordFilePath);
    
    // Set the button's state back to "record"
    _btnRecord.titleLabel.text = @"Record";
    _btnPlay.enabled = YES;
}

- (void) stopPlayQueue
{
    player->StopQueue();
    _btnRecord.enabled = YES;
}

- (void) pausePlayQueue
{
    player->PauseQueue();
    playbackWasPaused = YES;
}

# pragma mark Notification routines
- (void) handleRouteChange: (NSNotification*) notification {
    NSLog(@"handleRouteChange");
    
    //source: https://github.com/LaiFengiOS/LFLiveKit/blob/master/LFLiveKit/capture/LFAudioCapture.m
    AVAudioSession *session = [ AVAudioSession sharedInstance ];
    NSString *seccReason = @"";
    NSInteger reason = [[[notification userInfo] objectForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    //  AVAudioSessionRouteDescription* prevRoute = [[notification userInfo] objectForKey:AVAudioSessionRouteChangePreviousRouteKey];
    switch (reason) {
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            seccReason = @"The route changed because no suitable route is now available for the specified category.";
            break;
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            seccReason = @"The route changed when the device woke up from sleep.";
            break;
        case AVAudioSessionRouteChangeReasonOverride:
            seccReason = @"The output route was overridden by the app.";
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            seccReason = @"The category of the session object changed.";
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            seccReason = @"The previous audio output path is no longer available.";
            break;
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            seccReason = @"A preferred new audio output path is now available.";
            break;
        case AVAudioSessionRouteChangeReasonUnknown:
        default:
            seccReason = @"The reason for the change is unknown.";
            break;
    }
    NSLog(@"handleRouteChange reason is %@", seccReason);
    
    AVAudioSessionPortDescription *input = [[session.currentRoute.inputs count] ? session.currentRoute.inputs : nil objectAtIndex:0];
    if (input.portType == AVAudioSessionPortHeadsetMic) {
        
    }
}

- (void)playbackQueueStopped:(NSNotification *)note {
    _btnPlay.titleLabel.text = @"Play";
    _btnRecord.enabled = YES;
}

- (void)playbackQueueResumed:(NSNotification *)note {
    _btnPlay.titleLabel.text = @"Stop";
    _btnRecord.enabled = NO;
}

#pragma mark - File related code
char *OSTypeToStr(char *buf, OSType t)
{
    char *p = buf;
    char str[4] = {0};
    char *q = str;
    *(UInt32 *)str = CFSwapInt32(t);
    for (int i = 0; i < 4; ++i) {
        if (isprint(*q) && *q != '\\')
            *p++ = *q++;
        else {
            sprintf(p, "\\x%02x", *q++);
            p += 4;
        }
    }
    *p = '\0';
    return buf;
}

-(void)setFileDescriptionForFormat: (CAStreamBasicDescription)format withName:(NSString*)name
{
    char buf[5];
    const char *dataFormat = OSTypeToStr(buf, format.mFormatID);
    NSString* description = [[NSString alloc] initWithFormat:@"(%u ch. %s @ %g Hz)", (unsigned int)(format.NumberChannels()), dataFormat, format.mSampleRate, nil];
    _statusLabel.text = description;
}

@end
