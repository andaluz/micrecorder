//
//  MicRecorderVC.h
//  MicRecorder
//
//  Created by Nordin on 13/04/2018.
//  Copyright © 2018 AppPlanet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQRecorder.h"
#import "AQPlayer.h"



@interface MicRecorderVC : UIViewController {
    AQPlayer* player;
    AQRecorder* recorder;
    
    CFStringRef recordFilePath;
    
    BOOL playbackWasInterrupted;
    BOOL playbackWasPaused;
}


@property (nonatomic, readonly)         AQPlayer *player;
@property (nonatomic, readonly)         AQRecorder *recorder;

@property (nonatomic)                   BOOL playbackWasInterrupted;
@property (nonatomic, assign)           BOOL inBackground;


@property (weak, nonatomic) IBOutlet UIButton *btnRecord;
@property (weak, nonatomic) IBOutlet UIButton *btnStop;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *numPacketsLabel;
@property (weak, nonatomic) IBOutlet UILabel *codecLabel;



@end
